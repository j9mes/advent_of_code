valid_pws_1 = 0
valid_pws_2 = 0

with open('data/advent_2.txt') as file:
    for entry in list(file):
        letter_list = []
        letter = entry[(entry.find(':'))-1]
        first_number = entry[:entry.find('-')]
        second_number = entry[entry.find('-')+1:entry.find(' ')]
        password = entry[entry.find(':')+2:len(entry)-1]


        if int(first_number) <= password.count(letter) <= int(second_number):
            valid_pws_1 += 1

        letter_list.extend([password[int(first_number)-1],
                            password[int(second_number)-1]]
                           )

        if letter_list.count(letter) == 1:
            valid_pws_2 += 1

print(valid_pws_1)
print(valid_pws_2)