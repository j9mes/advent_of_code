import numpy


def prep_line(lin, xco):
    lin = lin.strip()
    while len(lin) <= xco:
        lin += lin
    return lin

x_coords_list = [1, 3, 5, 7]
x_coords_tally = [0, 0, 0, 0]
tree_list = [0, 0, 0, 0]
right_1_down_2 = 0
line_index = 0
x_coord = 0

with open('data/tree_slope.txt') as file:

    for line in file:
        for coord in x_coords_list:
            line = prep_line(line, x_coords_tally[x_coords_list.index(coord)])
            if line[x_coords_tally[x_coords_list.index(coord)]] == '#':
                tree_list[x_coords_list.index(coord)] += 1
            x_coords_tally[x_coords_list.index(coord)] += coord

        if line_index % 2 == 0:
            line = prep_line(line, x_coord)
            if line[x_coord] == '#':
                right_1_down_2 += 1
            x_coord += 1
        line_index += 1

tree_list.append(right_1_down_2)
print(numpy.prod(tree_list))
